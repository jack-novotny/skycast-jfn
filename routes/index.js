var express = require('express');
var router = express.Router();
const moment = require('moment');



const apiController = require('../ApiControllers/apiController.js');

/* GET home page. */
router.get('/', apiController.locationCall, function(req, res, next) {
  res.render('index', 
  	{ title: 'Skycast Weather, Inc.',
  	moment: moment,
  	temp: res.locals.temp,
    feelsLike: res.locals.feelsLike,
  	summary: res.locals.summary,
  	forecast: res.locals.forecast,
    icon: res.locals.icon
  });
});

router.post('/searchResults', apiController.findWeatherCall, function(req, res, next) {
  res.render('searchResults', {
    city: res.locals.city,
    state: res.locals.state,
    title: 'Skycast Weather, Inc.',
    temp: res.locals.temp,
    feelsLike: res.locals.feelsLike,
    summary: res.locals.summary,
    forecast: res.locals.forecast,
    icon: res.locals.icon,
    moment: moment
   });
});

router.post('/timeMachineResults', apiController.findHistoricWeatherCall, function(req, res, next) {
  res.render('timeMachineResults', {
    city: res.locals.city,
    state: res.locals.state,
    title: 'Skycast Weather, Inc.',
    date: res.locals.date,
    temp: res.locals.temp,
    icon: res.locals.icon,
    summary: res.locals.summary
   });
});

router.get('/timeMachine', function(req, res, next) {
  console.log("Time machine page");
  res.render('timeMachine', {
    title: 'Skycast Weather, Inc.'
  });
});






module.exports = router;
