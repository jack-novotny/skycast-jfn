//apiController.js
var axios = require('axios');
require('dotenv').config();
var timestamp = require('unix-timestamp');

//const LOCATION_KEY = process.env.LOCATION_KEY;
//const DARKSKY_KEY = process.env.DARKSKY_KEY;
const LOCATION_KEY = 'AIzaSyB2M9XWxvSkNQ19IHwrRwGW8WkEqv0x0Ds'; //process.env.LOCATION_KEY w/ config vars
const DARKSKY_KEY = '4e61604a608fbba1d7ec2fbddd494ecf'; //process.env.DARKSKY_KEY w/ config vars


function locationCall (req, res, next) {
	console.log(LOCATION_KEY);
  console.log(DARKSKY_KEY);
  var url = 'https://www.googleapis.com/geolocation/v1/geolocate?key=' + LOCATION_KEY;
  axios.post(url)
  .then((response) => {
    var lat = response.data.location.lat;
    var lon = response.data.location.lng;
    var dsurl = 'https://api.darksky.net/forecast/' + DARKSKY_KEY + '/' + lat + ',' + lon;
    return axios.get(dsurl)
    }).then((response) => {
      res.locals.temp = Math.round(response.data.currently.temperature);
      res.locals.feelsLike = Math.round(response.data.currently.apparentTemperature);
      res.locals.summary = response.data.currently.summary;
      res.locals.forecast = response.data.daily.data;
      res.locals.icon = response.data.currently.icon;
      return next();
    }).catch((e) => {
    	console.log(e);
    });
}

function findWeatherCall (req, res, next) {
	var location = req.body.location;
  var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + location + '&key=' + LOCATION_KEY;

	axios.get(url)
  .then((response) => {
    var lat = response.data.results[0].geometry.location.lat;
    var lon = response.data.results[0].geometry.location.lng;
    if (response.data.results[0].address_components.length === 6) {
    	res.locals.city = response.data.results[0].address_components[2].long_name;
    	res.locals.state = response.data.results[0].address_components[4].long_name;
    	console.log("length 6");
    } 
    else if (response.data.results[0].address_components.length === 5) {
    	res.locals.city = response.data.results[0].address_components[1].long_name;
    	res.locals.state = response.data.results[0].address_components[3].long_name;
    	console.log("length 5");
    } 
    else if (response.data.results[0].address_components.length === 4) {
    	res.locals.city = response.data.results[0].address_components[1].long_name;
    	res.locals.state = response.data.results[0].address_components[2].long_name;
    	console.log("length 4");
    }
    else {
    	res.locals.city = response.data.results[0].address_components[0].long_name;
    	res.locals.state = response.data.results[0].address_components[1].long_name;
    	console.log("length less than 4");
    }

    var dsurl = 'https://api.darksky.net/forecast/' + DARKSKY_KEY + '/' + lat + ',' + lon;
    
    return axios.get(dsurl)
  }).then((response) => {
      res.locals.temp = Math.round(response.data.currently.temperature);
      res.locals.feelsLike = Math.round(response.data.currently.apparentTemperature);
      res.locals.summary = response.data.currently.summary;
      res.locals.forecast = response.data.daily.data;
      res.locals.icon = response.data.currently.icon;
      return next();
     }).catch((e) => {
      console.log(e);
  });

}

function findHistoricWeatherCall (req, res, next) {
  var location = req.body.location;
  var date = req.body.date;
  res.locals.date = date;
  var unix_date = timestamp.fromDate(date);
  var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + location + '&key=' + LOCATION_KEY;
  axios.get(url)
  .then((response) => {
    var lat = response.data.results[0].geometry.location.lat;
    var lon = response.data.results[0].geometry.location.lng;
    if (response.data.results[0].address_components.length === 6) {
      res.locals.city = response.data.results[0].address_components[2].long_name;
      res.locals.state = response.data.results[0].address_components[4].long_name;
      console.log("length 6");
    } 
    else if (response.data.results[0].address_components.length === 5) {
      res.locals.city = response.data.results[0].address_components[1].long_name;
      res.locals.state = response.data.results[0].address_components[3].long_name;
      console.log("length 5");
    } 
    else if (response.data.results[0].address_components.length === 4) {
      res.locals.city = response.data.results[0].address_components[1].long_name;
      res.locals.state = response.data.results[0].address_components[2].long_name;
      console.log("length 4");
    }
    else {
      res.locals.city = response.data.results[0].address_components[0].long_name;
      res.locals.state = response.data.results[0].address_components[1].long_name;
      console.log("length less than 4");
    }

    var dsurl = 'https://api.darksky.net/forecast/' + DARKSKY_KEY + '/' + lat + ',' + lon + ',' + unix_date;

    return axios.get(dsurl)
  }).then((response) => {
    res.locals.temp = Math.round(response.data.currently.temperature);
    res.locals.summary = response.data.currently.summary;
    res.locals.icon = response.data.currently.icon;
    return next();
    }).catch((e) => {
      console.log(e);
    });

}


module.exports = {
	locationCall,
	findWeatherCall,
  findHistoricWeatherCall
};